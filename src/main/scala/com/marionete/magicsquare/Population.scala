package com.marionete.magicsquare

import scala.util.Random
import scala.util.Random._

case class Population(individuals: Seq[Individual]) {
  def sort: Population = this.copy(individuals = this.individuals.sorted)

  def calcFitness: Population = 
    this.copy(individuals = this.individuals.par.map(_.calcFitness).seq)

  def populationFitness(percentile: Double):  Double = {
    val percentile80 = (percentile*this.individuals.size).toInt
    this.sort.individuals.take(percentile80).map(_.fitness).flatten.sum.toDouble / percentile80.toDouble
  }

/** Tournament selection selects its parents by running a series of "tournaments".
  * First, individuals are randomly selected from the population and entered into a
  * tournament. Next, these individuals can be thought to compete with each other
  * by comparing their fitness values, then choosing the individual with the highest
  * fitness for the parent.
  */
  def tournamentSelection(n: Int, r: Random): Individual =
    Population((r.shuffle(this.individuals)).take(n)).individuals.head

  def selectParents(n: Int, t: Int): Seq[(Individual, Individual)] = {
  // n -> number of children, t -> tournament size
    import scala.annotation.tailrec
    @tailrec
    def loop(n: Int, acc: Seq[(Individual, Individual)]): Seq[(Individual, Individual)] = 
    n match {
      case 0 => acc
      case _ => loop(n-1, (tournamentSelection(t, new Random()),
	                   tournamentSelection(t, new Random()))+:acc)
    }

    loop(n, Seq())
  }

  def offspring(parents: Seq[(Individual,Individual)], m: Double, c: Double): Seq[Individual] = 
  // parents -> list of parents, m -> mutation rate, c -> crossover rate
    parents.par.map{ case (p1,p2) => p1.crossover(p2, c, new Random()).mutation(m, new Random()) }.seq

  def newGeneration(e: Int, t: Int, m: Double, c: Double): Population = {
  // e -> elite, t -> tournament size, m -> mutation rate, c -> crossover rate
    val elitePopulation = this.sort.individuals.take(e)
    val offspringPopulation = offspring(selectParents(this.individuals.size-e, t), m, c)
    Population(offspringPopulation ++ elitePopulation).calcFitness
  }
}

object Population {
  def apply(populationSize: Int, 
            chromosomeSize: Int,
	    r: Random) = {
    def individuals(i: Int, list: Seq[Individual]): Seq[Individual] = 
      i match {
        case 0 => list
	case _ => individuals(i-1, Individual(chromosomeSize, r) +: list)
    }

    new Population(individuals(populationSize, Seq()))
  }
}
