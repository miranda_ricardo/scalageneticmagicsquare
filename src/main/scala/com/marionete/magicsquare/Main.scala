package com.marionete.magicsquare

object Main extends App {
  def helpMessage: Unit = {
    println("To run this program type:")
    println("geneticMagicSquares, max number of iterations, magic square size, size of the population, mutation rate, crossover, elite, tournament size, percentile for population fitness computationi, interval between CLI outputs")
    println("geneticMagicSquares Int Int Int Double Double Int Int Double Int")
  }

  def wrongNbrArgs: Unit = {
    println("Invalid input, invalid number of arguments")
    println("Type geneticMagicSquares --help")
  }

  def main: Unit = {
    val iter = args(0).toInt
    val sideSize = args(1).toInt
    val popSize = args(2).toInt
    val mutationRate = args(3).toDouble
    val crossoverRate = args(4).toDouble
    val elite = args(5).toInt
    val tournamentSize = args(6).toInt
    val percentile = args(7).toDouble
    val output = args(8).toInt

    import scala.annotation.tailrec
    @tailrec
    def loop(n: Int, iterToGo: Int, population: Population, acc: Seq[(Int, Int, Double, Int, Seq[Int])]): 
      Seq[(Int, Int, Double, Int, Seq[Int])] = iterToGo match {
    // n -> curent iteration, iterToGo -> remaining iterations,
    // population -> current population,
    // acc -> Seq[(n, bestFitness, fitnessPopulation, PopulationSize, chromosome)]
    // returns -> Seq[(n, bestFitness, fitnessPopulation, PopulationSize, chromosome)]
      case 0 => acc
      case _ =>
        val newGeneration = 
          population.newGeneration(elite, tournamentSize, mutationRate, crossoverRate)

        newGeneration.calcFitness
 
        val bestFitness = 
          newGeneration.sort.individuals.head.fitness
            .getOrElse(throw new RuntimeException("Error fetching fitness."))

        val result = (n,
                      bestFitness, 
                      newGeneration.populationFitness(percentile), 
                      newGeneration.individuals.size,
                      newGeneration.sort.individuals.head.chromosome)

	val percent = 100.0 - n.toDouble/iter.toDouble*100.0
	if (n % output == 0) println(s"Remainig: ${percent}%, best individual: $bestFitness , population size = ${population.individuals.size}") 

        loop(n+1, 
             if (bestFitness == 0) { 0 } else { iterToGo-1 }, 
             newGeneration, 
             result+:acc)
    }

    import scala.util.Random
    val iniPopulation = Population(populationSize = popSize,
	                           chromosomeSize = sideSize*sideSize,
				   new Random()).calcFitness

    import breeze.linalg._
    println("Initial solution is:")
    println(MagicSquare(iniPopulation.sort.individuals.head.chromosome).m)
    println(s"With fitness: ${iniPopulation.sort.individuals.head.fitness}")

    val result = loop(0, iter, iniPopulation, Seq())

    println("Final solution is:")
    println(MagicSquare(result.head._5).m)
    println(s"With fitness: ${result.head._2}")
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(result.map(_._1), result.map(_._2), name="Best individual")
    plt += plot(result.map(_._1), result.map(_._3.toInt), name="Population")
    plt.xlabel = "Iterations"
    plt.ylabel = "Fitness"
    plt.title = s"Magic square with size $sideSize fitness"
    plt.legend = true
    fig.refresh()

    println("Program terminated")
    val t0 = System.nanoTime()
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) + "ns")

  }

  args match {
    case Array() => helpMessage
    case Array("--help",_*) => helpMessage
    case xs if xs.size == 9 => main
    case _ => wrongNbrArgs
  }
}
