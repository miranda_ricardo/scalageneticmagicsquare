package com.marionete.magicsquare

import breeze.linalg._
import breeze.linalg.functions._
import breeze.numerics._

case class MagicSquare(m: DenseMatrix[Int]) {
  def squareDiferences: Int = {
    def sqr(x: Int) = x*x

    def lines: List[Int] = {
      List(trace(this.m),
           trace(this.m(*,::).map(reverse(_)))
		  ) ++ (sum(this.m(*,::))).toArray.toList ++ (sum(this.m(::,*))).t.toArray.toList
    }

    import scala.annotation.tailrec
    @tailrec
    def loop(xs: List[Int], acc: Int): Int = xs match {
      case Seq(x) => acc
      case x::y::xs => loop(y::xs, acc + sqr(x-y))
    }

    loop(lines, 0)
  }
}

object MagicSquare {
  def apply(chromosome: Seq[Int]) = {
    val n = sqrt(chromosome.size).toInt
    new MagicSquare(DenseMatrix(chromosome.grouped(n).toSeq:_*))
  }
}
