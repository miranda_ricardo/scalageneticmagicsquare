package com.marionete.magicsquare

import org.scalatest._
import org.scalatest.Matchers._

import scala.util.Random

class PopulationTest extends WordSpec with Matchers {
  "A new Population" when {
    "of size 1 with Individual chromosome size 1" should {
      "the chromosome of the lonesome individual == Seq(0)" in {
        Population(1,1,new Random()).individuals(0).chromosome shouldBe Seq(0)
      }
    }

    "of size 10 with Individual chromosome size 1" should {
      "the chromosome of the 5th individual == Seq(0)" in {
        Population(10,1,new Random()).individuals(4).chromosome shouldBe Seq(0)
      }
    }

    "of size 5 with Individual chromosome size 3 and Random seed 0" should {
      "the chromosome of the first element of the Population == Seq(2,1,0)" in {
        Population(5,3,new Random(0)).individuals(0).chromosome should contain theSameElementsInOrderAs(Vector(2,1,0))
      }
    }

    "of size 5" should {
      "be a Population of size 5" in {
        Population(5,3,new Random(0)).individuals.size shouldBe 5
      }
    }

    "of size 1 with Individual with chromosome Seq(0,1,5)" should {
      "the chromosome of the element of the Population == Seq(0,1,5)" in {
        Population(Seq(Individual(Seq(0,1,5)))).individuals(0).chromosome should contain theSameElementsInOrderAs(Vector(0,1,5))
      }
    }

    "of size 2 with Individual with chromosomes Seq(0,1,5)" should {
      "the chromosome of the 2nd element of the Population == Seq(0,1,5)" in {
        Population(Seq(Individual(Seq(0,1,5)),
                       Individual(Seq(0,1,5)))).individuals(1).chromosome should contain theSameElementsInOrderAs(Vector(0,1,5))
      }
    }
  }

  "A Population" when {
    "of size 1" should {
      "with chromosome Seq(1,2,3,4) have fitness Some(33)" in {
        Population(Seq(Individual(Seq(1,2,3,4))))
          .calcFitness
          .individuals(0)
	  .fitness shouldBe Some(33)
      }

      "with chromosome Seq(1,2,3,4) retain same order" in {
        Population(Seq(Individual(Seq(1,2,3,4))))
          .calcFitness
          .sort
          .individuals(0)
          .chromosome should contain theSameElementsInOrderAs(Vector(1,2,3,4))
      }
    }
    
    "of size 2, sorted" should {
      "with chromosomes A = Seq(1,2,3,4) and B = Seq(1,2,3,4,5,6,7,8,9), B be 2nd" in {
        Population(Seq(Individual(Seq(1,2,3,4)),
                       Individual(Seq(1,2,3,4,5,6,7,8,9))))
          .calcFitness
          .sort
          .individuals(1)
          .chromosome should contain theSameElementsInOrderAs(Vector(1,2,3,4,5,6,7,8,9))
      }
    }
    
    "of size 3, chromosomes A = Seq(1,2,3,4), B = Seq(1,2,3,4,5,6,7,8,9) and C = Seq(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)" should {
      "with tournament size 2, return A" in {
        Population(Seq(Individual(Seq(1,2,3,4)),
                       Individual(Seq(1,2,3,4,5,6,7,8,9)),
                       Individual(Seq(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16))))
          .calcFitness
          .tournamentSelection(2, new Random(0))
          .chromosome should contain theSameElementsInOrderAs(Vector(1,2,3,4,5,6,7,8,9))
      }
    }
    
    "of size 3 with tournament size 2 and number of children 2" should {
      "return a sequence of parents with size 2" in {
        Population(Seq(Individual(Seq(1,2,3,4)),
                       Individual(Seq(1,2,3,4,5,6,7,8,9)),
                       Individual(Seq(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16))))
          .calcFitness
          .selectParents(2,2)
          .size shouldBe 2
      }
    }
    
    "of size 3" should {
      "return a Population of size 3" in {
        Population(Seq(Individual(Seq(1,2,3,4)),
                       Individual(Seq(1,2,3,4)),
                       Individual(Seq(1,2,3,4))))
          .calcFitness
          .newGeneration(3,2,0.5,0.5)
          .individuals
          .size shouldBe 3
      }
    }
  }
    
  "A list of parents" when {
    "of size 1 with crossover = 0.5 and mutation = 0.5" should {
      "return a Population of size 1" in {
        Population(Seq())
          .offspring(Seq((Individual(Seq(1,2,3,4)), Individual(Seq(1,2,3,4)))), 0.5, 0.5)
          .size shouldBe 1
      }
    }
  }
}
